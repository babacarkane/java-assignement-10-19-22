class TryingMethods {
    /*
     * TryingMethods is an experimentation class which aim
     * to test class insights about:
     * Class
     * Object
     * Methods
     * If statement
     * For loop
     */

    public static void main(String[] args) {
        // Class instanciation
        TryingMethods testObject = new TryingMethods();

        // Dynamic Methods dayOfTheWeek test
        String monday = testObject.dayOfTheWeek(1);
        String wednesday = testObject.dayOfTheWeek(3);
        String friday = testObject.dayOfTheWeek(5);

        // Output
        System.out.println(monday);
        System.out.println(wednesday);
        System.out.println(friday);

        /* ----------------------------------------------------------------------- */

        // Dynamic method whoIsYourBuddy test
        testObject.whoIsYourBuddy("TECH");
        testObject.whoIsYourBuddy("BUSINESS");
        testObject.whoIsYourBuddy("COM");
        testObject.whoIsYourBuddy("NINJUTSU");

        /* ----------------------------------------------------------------------- */

        // Static method oneTo1000Sum test
        System.out.println(zeroTo1000Sum());

        /* ----------------------------------------------------------------------- */

        // Static method evenNumbers12To103Sum test
        System.out.println(evenNumbers12To103Sum());
    }

    String dayOfTheWeek(int dayNumber) {
        String dayString;
        switch (dayNumber) {
            case 1:
                dayString = "Monday";
                break;
            case 2:
                dayString = "Tuesday";
                break;
            case 3:
                dayString = "Wednesday";
                break;
            case 4:
                dayString = "Thursday";
                break;
            case 5:
                dayString = "Friday";
                break;
            case 6:
                dayString = "Saturday";
                break;

            default:
                dayString = "Sunday";
                break;
        }

        return dayString;
    }

    void whoIsYourBuddy(String skill) {
        String buddyName;
        if (skill == "TECH")
            buddyName = "Bright";
        else if (skill == "BUSINESS")
            buddyName = "Lady O";
        else if (skill == "COM")
            buddyName = "Afi";
        else {
            System.out.println("Unfortunately, we cannot find you buddy");
            return;
        }

        System.out.println("You buddy name is " + buddyName);
    }

    static int zeroTo1000Sum() {
        int sum = 0;
        int i = 0;
        while (i < 1000) {
            sum += i++;
        }

        return sum;
    }

    static int evenNumbers12To103Sum() {
        int sum = 0;
        for (int i = 12; i < 103; i++) {
            if (i % 2 == 0)
                sum += i;
        }

        return sum;
    }
}